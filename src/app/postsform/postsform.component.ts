import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-postsform',
  templateUrl: './postsform.component.html',
  styleUrls: ['./postsform.component.css']
})
export class PostsformComponent implements OnInit {

  constructor(private postsservice:PostsService, private router:Router, private route:ActivatedRoute) { }
  
 body:string;
  title:string;
  id:string;
  author:string;
  isEdit:boolean = false;
buttonText:string = "Add Post";
  onSubmit(){ 
if(this.isEdit){
this.postsservice.updatePost(this.id, this.title, this.body)
}else{

    this.postsservice.addPost(this.title,this.author,this.body)
}
    this.router.navigate(['/posts']);
  }  
 

  ngOnInit() {
this.id = this.route.snapshot.params.id;
console.log(this.id);
if(this.id){
this.isEdit = true;
this.buttonText = "Update";
this.postsservice.getPost(this.id).subscribe(
  post => {
    console.log(post.data().author)
    console.log(post.data().title)
    this.author = post.data().author;
    this.title = post.data().title;
    this.body = post.data().body;
  }
)
}
  }

}
