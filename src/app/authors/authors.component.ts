import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorsService } from '../authors.service';



/**
 * @title List with selection
 */
@Component({
  selector: 'app-authors',
  styleUrls: ['./authors.component.css'],
  templateUrl: './authors.component.html',
})
export class AuthorsComponent implements OnInit {
  static SumValue(): any {
    throw new Error("Method not implemented.");
  }
 /* typesOfAuthors: object[] = [{id:1, name:'Lewis Carrol'}, {id:2, name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}, {id:4, name:'Mo'}, {id:5, name:'Sn'}];
  name;
  id;
*/
    panelOpenState = false;
    //books: any;
    authors$:Observable<any>;
    author:string;



 constructor(/*private route: ActivatedRoute*/private authorsservice:AuthorsService,private cms: AuthorsService) { }

 onSubmit(){
   this.authorsservice.addAuthors(this.author);
 }
  ngOnInit() {
    /*this.name = this.route.snapshot.params.name;
    this.id= this.route.snapshot.params.id;*/
    this.authors$ = this.authorsservice.getAuthors();
  }

}
