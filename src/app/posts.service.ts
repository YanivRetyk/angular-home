import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './interface/post';
import { AngularFirestore } from '@angular/fire/firestore';
import { Users } from './interface/users';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {


apiUrl='https://jsonplaceholder.typicode.com/posts/';
//apiUrlUser='https://jsonplaceholder.typicode.com/users/';

  constructor(private http: HttpClient, private db: AngularFirestore) { }
/* GETING FROM JSON
getPosts(){
  return this.http.get<Post[]>(this.apiUrl)
}
*/


//getUser(){
 // return this.http.get<Users[]>(this.apiUrlUser);
//}

deletePost(id:string){
  this.db.doc(`posts/${id}`).delete();
}
getPost(id:string):Observable<any>{
  return this.db.doc(`posts/${id}`).get();
}
updatePost(id:string, title:string, body:string){
  this.db.doc(`posts/${id}`).update({
  title:title,
  body:body
  })
}

//get from db
getPosts():Observable<any[]>{
  return this.db.collection('posts').valueChanges({idField:'id'});
   }

addPost(title:String,  author:String, body:String){
  const post = {title:title, author:author, body:body}
  this.db.collection('posts').add(post)  
}  

}
