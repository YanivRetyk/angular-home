import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs'
import { Post } from '../interface/post';
import { PostsService } from '../posts.service';
import { UsersService } from '../users.service';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { Users } from '../interface/users';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  deletePost(id:string){
    this.postsService.deletePost(id);
  }

  //geting posts from json not from db
  //posts$: Post[];
  users$: Users[];

  title:String;
  body:String;
  author:String;
  ans:String;

  //geting posts from db not from json
  posts$:Observable<any>;

  constructor(private route: ActivatedRoute, private usersService: UsersService, private postsService: PostsService) { }
 
 
  /*method that compares id from users.Json to userid from posts.Json
  myFunc(){
    for (let index = 0; index < this.posts$.length; index++) {
      for (let i = 0; i < this.users$.length; i++) {
        if (this.posts$[index].userId==this.users$[i].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.author = this.users$[i].name;
          this.postsService.addPost(this.body,this.author,this.title);
          
        }
       }
    }
    this.ans ="The data retention was successful"
  }
*/

  ngOnInit() {
//geting posts from db
    this.posts$ = this.postsService.getPosts();
//geting posts from json
     //this.usersService.getUsers() .subscribe(data => this.users$ = data)
    //this.postsService.getPosts() .subscribe(data => this.posts$ = data)
   
  
  }
  onSubmit(){ 
    //this.postsService.addPost(this.title,this.body)
    //this.usersService.addUser(this.name)
    
  } 

}