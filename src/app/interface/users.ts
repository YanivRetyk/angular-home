export interface Users {
    id:number,
    name:String ,
    username: String,
    email: String,
    street: String,
    suite:String,
    city: String,
    zipcode: String,
    lat: number,
    lng: number
    phone: String,
    website: String
}
