export interface Post {
    username: String;
    userId:number;
    id: number;
    title: String;
    body: String;

}
