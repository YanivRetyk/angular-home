export interface BlogRaw {
    userId:number
    id: number
    title: String
    body: String
}
