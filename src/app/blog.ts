export interface Blog {
    userId:number;
    id: number;
    title: String;
    body: String;
}
