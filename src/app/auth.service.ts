import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interface/user';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth) {
    this.user =this.afAuth.authState;
   }

   signup(email:string, password:string){
     this.afAuth
         .auth
         .createUserWithEmailAndPassword(email,password)
         .then(res => 
          console.log('Succesful Signup', res)
          );
   }

   Logout(){
    this.afAuth.auth.signOut();
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          res => console.log('Succesful Login',res)
        );
  }
}
